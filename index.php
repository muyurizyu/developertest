<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Developer Test Phase 1</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400&display=swap" rel="stylesheet">

    <style>
        :root{
            --blue: #0094f8;
            --green: #017d27;
            --darkgrey: #cbcbcb;
            --grey: #ededed;
            --white: #ffffff;
        }
        *{
            padding: 0;
            margin: 0;
            box-sizing: border-box;
            font-family: 'Roboto', sans-serif;
        }
        .my-wrapper{
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%;
            height: 100vh;
            background-color: var(--grey);
        }
        .my-col{
            display: flex;
            flex-flow: column wrap;
            align-items: center;
        }
        .my-row{
            display: flex;
            flex-flow: row wrap;
            align-items: center;
        }
        span.green{
            color: var(--green);
        }
        span.blue{
            color: var(--blue);
        }
        .my-button-blue{
            display: inline-block;
            min-width: 100px;
            padding: 10px;
            margin: 0 10px 0 10px;
            border: none;
            color: var(--white);
            background-color: var(--blue);
        }
        .my-button-green{
            display: inline-block;
            min-width: 100px;
            padding: 10px;
            margin: 0 10px 0 10px;
            border: none;
            color: var(--white);
            background-color: var(--green);
        }
        .my-button-blue:focus{
            outline: 3px solid var(--white);
            box-shadow: 6px 6px 3px var(--darkgrey);
        }
    </style>
</head>
<body>
<div class="my-wrapper">
    <div class="my-col">
        <h4>Today is <span class="green"><?php echo date("l"); ?></span></h4><br>
        <div class="my-row">
        <?php
        $day = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $count = count($day);
        
        for ($x = 0; $x < $count; $x++) {
            if ($day[$x] == date("l")) {
                ?>
                <button class="my-button-green" onclick="document.getElementById('selected').innerHTML='<?php echo $day[$x] ?>'"><?php echo $day[$x] 
                ?></button>
                <?php
            }
            else{
                ?>
                <button id="button-2" class="my-button-blue" onclick="document.getElementById('selected').innerHTML='<?php echo $day[$x] ?>'"><?php echo $day[$x] ?></button>
        <?php
            }
        }
        ?>
        </div>
        <br><h4>Selected day is <span id="selected" class="blue"></span></h4><br>
    </div>
</div>
</body>
<html>